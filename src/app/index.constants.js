/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('kramnicya')
    .constant('malarkey', malarkey)
    .constant('moment', moment);

})();
