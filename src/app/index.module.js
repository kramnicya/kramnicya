(function() {
  'use strict';

  angular
    .module('kramnicya', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngMessages', 'ngAria', 'restangular', 'ngRoute', 'toastr']);

})();
