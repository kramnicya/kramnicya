(function() {
  'use strict';

  angular
    .module('kramnicya')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
